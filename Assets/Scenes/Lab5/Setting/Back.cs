using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace Korawan.GameDev4.UIToolkit
{
    public class Back : MonoBehaviour
    {
        private UIDocument _uiDocument;
        
        private VisualElement _backButton;
        
        // Start is called before the first frame update
        void Awake()
        {
            _uiDocument = FindObjectOfType<UIDocument>();
            _backButton = _uiDocument.rootVisualElement.Query<Button>("back-button");
        }

        private void OnEnable()
        {
            _backButton.RegisterCallback<ClickEvent>(OnBackButtonMouseDownEvent);
        }
        
        private void OnBackButtonMouseDownEvent(ClickEvent evt)
        {
            SceneManager.LoadSceneAsync("5.1");
        }

        private void OnDisable()
        {
            _backButton.UnregisterCallback<ClickEvent>(OnBackButtonMouseDownEvent);
        }
    }
}