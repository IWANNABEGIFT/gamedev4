using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace Korawan.GameDev4.UIToolkit
{
    public class Credits : MonoBehaviour
    {
        private UIDocument _uiDocument;
        
        private VisualElement _creditsButton;
        
        // Start is called before the first frame update
        void Awake()
        {
            _uiDocument = FindObjectOfType<UIDocument>();
            _creditsButton = _uiDocument.rootVisualElement.Query<Button>("credits-button");
        }

        private void OnEnable()
        {
            _creditsButton.RegisterCallback<ClickEvent>(OnCreditsButtonMouseDownEvent);
        }
        
        private void OnCreditsButtonMouseDownEvent(ClickEvent evt)
        {
            SceneManager.LoadSceneAsync("Credits");
        }
        

        private void OnDisable()
        {
            _creditsButton.UnregisterCallback<ClickEvent>(OnCreditsButtonMouseDownEvent);
        }
    }
}