using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace Korawan.GameDev4.UIToolkit
{
    public class MainMenuEventHandler : MonoBehaviour
    {
        private UIDocument _uiDocument;

        private VisualElement _settingButton;
        
        // Start is called before the first frame update
        void Awake()
        {
            _uiDocument = FindObjectOfType<UIDocument>();
            _settingButton = _uiDocument.rootVisualElement.Query<Button>("setting-button");
        }

        private void OnEnable()
        {
            _settingButton.RegisterCallback<ClickEvent>(OnSettingButtonMouseDownEvent);
        }

        private void OnSettingButtonMouseDownEvent(ClickEvent evt)
        {
            SceneManager.LoadSceneAsync("Setting");
        }
        

        private void OnDisable()
        {
            _settingButton.UnregisterCallback<ClickEvent>(OnSettingButtonMouseDownEvent);
        }
    }
}