using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace Korawan.GameDev4.UIToolkit
{
    public class Backk : MonoBehaviour
    {
        private UIDocument _uiDocument;
        
        private VisualElement _back2Button;
        
        // Start is called before the first frame update
        void Awake()
        {
            _uiDocument = FindObjectOfType<UIDocument>();
            _back2Button = _uiDocument.rootVisualElement.Query<Button>("back2-button");
        }

        private void OnEnable()
        {
            _back2Button.RegisterCallback<ClickEvent>(OnBackButtonMouseDownEvent);
        }
        
        private void OnBackButtonMouseDownEvent(ClickEvent evt)
        {
            SceneManager.LoadSceneAsync("5.1");
        }

        private void OnDisable()
        {
            _back2Button.UnregisterCallback<ClickEvent>(OnBackButtonMouseDownEvent);
        }
    }
}